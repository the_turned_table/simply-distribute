﻿using Autofac;
using Distribute.IntegrationEvents.Events;
using DistributedCommon.EventBus;
using DistributedCommon.EventBus.Events;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.EventBusRabbitMQ
{
    public class EventBusServiceProvider
    {
        private Dictionary<string, EventBusRabbitMQ> eventBusDictionary = new Dictionary<string, EventBusRabbitMQ>();

        private int distributionLevel;

        public EventBusServiceProvider(int distributionLevel)
        {
            this.distributionLevel = distributionLevel;
        }

        public void AddEventBus(string name, EventBusRabbitMQ eventBus)
        {
            eventBusDictionary.Add(name, eventBus);
        }

        public EventBusRabbitMQ GetEventBus(string name)
        {
            return eventBusDictionary[name];
        }

        private List<string> GetKeysOfEventBus()
        {
            return new List<string>(eventBusDictionary.Keys);
        }

        public List<string> GetAvailableNodes()
        {
            var list = GetKeysOfEventBus();

            if (list.Count <= distributionLevel)
            {
                return list;
            }
            else
            {
                return list.GetRange(0, distributionLevel);
            }
        }

        public void PublishEventToAllEventBus(List<string> nodes, IntegrationEvent @event)
        {
            nodes.ForEach(node =>
            {
                eventBusDictionary[node].Publish(@event);
            });
        }

        public void RegisterHandleToAllNodes(List<string> nodes, Func<DistributedFileReadIntegrationEvent, Task<DistributedFileReadIntegrationEvent>> handle, Guid iD)
        {
            nodes.ForEach(node =>
            {
                eventBusDictionary[node].Handle = new EventBusRabbitMQ.DelegateHandle(handle);

                eventBusDictionary[node].SubscribeReply(iD);
            });
        }

        public List<DistributedFileReadIntegrationEvent> AwaitAllResults(List<string> nodes)
        {
            var list = new List<DistributedFileReadIntegrationEvent>();
            
            nodes.ForEach(async node =>
            {
                list.Add(await eventBusDictionary[node].HasChangedTask);
            });

            return list;
        }

        public static class Factory
        {
            public static EventBusRabbitMQ BuildEventBus(IRabbitMQPersistentConnection persistentConnection, ILogger<EventBusRabbitMQ> logger,
                                                            ILifetimeScope autofac, IEventBusSubscriptionsManager subsManager, string BrokerName, string queueName = null, bool isReplyNeeded = false, int retryCount = 5)
            {
                return new EventBusRabbitMQ(persistentConnection, logger, autofac, subsManager, BrokerName, queueName, isReplyNeeded, retryCount);
            }
        }
    }
}
