﻿using Autofac;
using Distribute.IntegrationEvents.Events;
using DistributedCommon.EventBus;
using DistributedCommon.EventBus.Abstractions;
using DistributedCommon.EventBus.Events;
using DistributedCommon.EventBus.Extensions;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DistributedCommon.EventBusRabbitMQ
{
    class EventBusRabbitMQNewImplementation: IEventBus, IDisposable
    {
        private readonly string BrokerName;
        private readonly string ResponseBrokerName;
        const string AutoFacScopeName = "secondary_autofac";

        private readonly IRabbitMQPersistentConnection _persistentConnection;
        private readonly ILogger<EventBusRabbitMQ> _logger;
        private readonly IEventBusSubscriptionsManager _subsManager;
        private readonly ILifetimeScope _autofac;
        private readonly int _retryCount;

        private IModel _consumerChannel;
        private IModel ReplyChannel;
        private string _queueName;
        private TaskCompletionSource<bool> _changedTaskSource = new TaskCompletionSource<bool>();
        public delegate Task DelegateHandle(DistributedFileReadIntegrationEvent fileReadIntegrationEvent);
        private DelegateHandle handle;
        public DelegateHandle Handle
        {
            get
            {
                return handle;
            }
            set
            {
                handle = value;
                HasDelegateBeenRegistered = true;
                IsReplyNeeded = true;
            }
        }

        public Task HasChangedTask => _changedTaskSource.Task;

        public bool HasChanged
        {
            set
            {
                _changedTaskSource.TrySetResult(value);
            }
        }

        private bool HasDelegateBeenRegistered = false;

        private bool IsReplyNeeded;

        public EventBusRabbitMQNewImplementation(IRabbitMQPersistentConnection persistentConnection, ILogger<EventBusRabbitMQ> logger,
            ILifetimeScope autofac, IEventBusSubscriptionsManager subsManager, string BrokerName, string queueName = null, bool isReplyNeeded = false, int retryCount = 5)
        {
            _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _subsManager = subsManager ?? new InMemoryEventBusSubscriptionsManager();
            _queueName = queueName;
            this.BrokerName = BrokerName;
            _autofac = autofac;
            _retryCount = retryCount;
            _consumerChannel = CreateConsumerChannel();
            _subsManager.OnEventRemoved += SubsManager_OnEventRemoved;
            IsReplyNeeded = isReplyNeeded;
            ResponseBrokerName = BrokerName + "-reply";
            ReplyChannel = CreateResponseChannel();
        }

        private void SubsManager_OnEventRemoved(object sender, string eventName)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            using (var channel = _persistentConnection.CreateModel())
            {
                channel.QueueUnbind(queue: _queueName,
                    exchange: BrokerName,
                    routingKey: eventName);

                if (_subsManager.IsEmpty)
                {
                    _queueName = string.Empty;
                    _consumerChannel.Close();
                }
            }
        }

        public void Publish(IntegrationEvent @event)
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            var policy = Policy.Handle<BrokerUnreachableException>()
                .Or<SocketException>()
                .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                    _logger.LogWarning(ex, "Could not publish event: {EventId} after {Timeout}s ({ExceptionMessage})", @event.Id, $"{time.TotalSeconds:n1}", ex.Message);
                });

            var eventName = @event.GetType().Name;

            _logger.LogTrace("Creating RabbitMQ channel to publish event: {EventId} ({EventName})", @event.Id, eventName);

            using (var channel = _persistentConnection.CreateModel())
            {
                _logger.LogTrace("Declaring RabbitMQ exchange to publish event: {EventId}", @event.Id);

                channel.ExchangeDeclare(exchange: BrokerName, type: "direct");

                var body = JsonSerializer.SerializeToUtf8Bytes(@event, @event.GetType(), new JsonSerializerOptions
                {
                    WriteIndented = true
                });

                policy.Execute(() =>
                {
                    var properties = channel.CreateBasicProperties();
                    properties.DeliveryMode = 2; // persistent

                    _logger.LogTrace("Publishing event to RabbitMQ: {EventId}", @event.Id);

                    channel.BasicPublish(
                        exchange: BrokerName,
                        routingKey: eventName,
                        mandatory: true,
                        basicProperties: properties,
                        body: body);
                });
            }
        }

        public void PublishResponse(IntegrationEvent @event, Guid iD, bool isReplyNeeded = false)
        {
            if (isReplyNeeded)
            {
                _logger.LogInformation("Reply was registered and will send to the reply channel to the following channel: " + ResponseBrokerName);

                if (!_persistentConnection.IsConnected)
                {
                    _persistentConnection.TryConnect();
                }

                var policy = Policy.Handle<BrokerUnreachableException>()
                    .Or<SocketException>()
                    .WaitAndRetry(_retryCount, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                    {
                        _logger.LogWarning(ex, "Could not publish event: {EventId} after {Timeout}s ({ExceptionMessage})", @event.Id, $"{time.TotalSeconds:n1}", ex.Message);
                    });

                var eventName = (@event as DistributedFileReadIntegrationEvent).Id.ToString();

                _logger.LogTrace("Creating RabbitMQ channel to publish event: {EventId} ({EventName})", @event.Id, eventName);

                using (var channel = _persistentConnection.CreateModel())
                {
                    _logger.LogTrace("Declaring RabbitMQ exchange to publish event: {EventId}", @event.Id);

                    channel.ExchangeDeclare(exchange: ResponseBrokerName, type: "direct");

                    var body = JsonSerializer.SerializeToUtf8Bytes(@event, @event.GetType(), new JsonSerializerOptions
                    {
                        WriteIndented = true
                    });

                    policy.Execute(() =>
                    {
                        var properties = channel.CreateBasicProperties();
                        properties.DeliveryMode = 2; // persistent

                        _logger.LogTrace("Publishing event to RabbitMQ: {EventId}", @event.Id);

                        _logger.LogInformation(
                            "Publishing to: exhangeKey:{ResponseBrokerName}, routingKey:{StringID}", ResponseBrokerName, iD.ToString());

                        channel.BasicPublish(
                            exchange: ResponseBrokerName,
                            routingKey: iD.ToString(),
                            mandatory: true,
                            basicProperties: properties,
                            body: body);
                    });
                }

            }
            else
            {
                _logger.LogInformation("No reply was registered, not doing anything.");
            }
        }

        public void SubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler
        {
            _logger.LogInformation("Subscribing to dynamic event {EventName} with {EventHandler}", eventName, typeof(TH).GetGenericTypeName());

            DoInternalSubscription(eventName: eventName);
            _subsManager.AddDynamicSubscription<TH>(eventName);
            StartBasicConsume();
        }

        public void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = _subsManager.GetEventKey<T>();
            DoInternalSubscription(eventName: eventName);

            _logger.LogInformation("Subscribing to event {EventName} with {EventHandler}", eventName, typeof(TH).GetGenericTypeName());

            _subsManager.AddSubscription<T, TH>();
            StartBasicConsume();
        }

        private void DoInternalSubscription(string eventName = "", bool subscribeToReply = false, Guid iD = new Guid())
        {
            var containsKey = subscribeToReply ?
                _subsManager.HasSubscriptionsForReply(iD) :
                _subsManager.HasSubscriptionsForEvent(eventName);

            if (!containsKey)
            {
                if (!_persistentConnection.IsConnected)
                {
                    _persistentConnection.TryConnect();
                }

                using (var channel = _persistentConnection.CreateModel())
                {
                    channel.QueueBind(queue: _queueName,
                                      exchange: subscribeToReply ? ResponseBrokerName : BrokerName,
                                      routingKey: subscribeToReply ? iD.ToString() : eventName);
                }
            }
        }

        public void Unsubscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>
        {
            var eventName = _subsManager.GetEventKey<T>();

            _logger.LogInformation("Unsubscribing from event {EventName}", eventName);

            _subsManager.RemoveSubscription<T, TH>();
        }

        public void UnsubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler
        {
            _subsManager.RemoveDynamicSubscription<TH>(eventName);
        }

        public void Dispose()
        {
            if (_consumerChannel != null)
            {
                _consumerChannel.Dispose();
            }

            _subsManager.Clear();
        }

        private void StartBasicConsume(bool basicReplyConsume = false)
        {
            _logger.LogTrace("Starting RabbitMQ basic consume");

            if (basicReplyConsume)
            {
                _logger.LogInformation("Reply is being registered");

                if (ReplyChannel != null)
                {
                    var consumer = new AsyncEventingBasicConsumer(ReplyChannel);

                    consumer.Received += Response_Consumer_Received;

                    ReplyChannel.BasicConsume(
                        queue: _queueName,
                        autoAck: false,
                        consumer: consumer);
                }
                else
                {
                    _logger.LogError("StartBasicConsume can't call on ReplyChannel == null");
                }

                return;
            }

            if (_consumerChannel != null)
            {
                var consumer = new AsyncEventingBasicConsumer(_consumerChannel);

                consumer.Received += Consumer_Received;

                _consumerChannel.BasicConsume(
                    queue: _queueName,
                    autoAck: false,
                    consumer: consumer);
            }
            else
            {
                _logger.LogError("StartBasicConsume can't call on _consumerChannel == null");
            }
        }

        private async Task Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
        {
            var eventName = eventArgs.RoutingKey;
            var message = Encoding.UTF8.GetString(eventArgs.Body.Span);

            try
            {
                if (message.ToLowerInvariant().Contains("throw-fake-exception"))
                {
                    throw new InvalidOperationException($"Fake exception requested: \"{message}\"");
                }

                await ProcessEvent(eventName, message);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "----- ERROR Processing message \"{Message}\"", message);
            }

            // Even on exception we take the message off the queue.
            // in a REAL WORLD app this should be handled with a Dead Letter Exchange (DLX). 
            // For more information see: https://www.rabbitmq.com/dlx.html
            _consumerChannel.BasicAck(eventArgs.DeliveryTag, multiple: false);
        }

        private async Task Response_Consumer_Received(object sender, BasicDeliverEventArgs eventArgs)
        {
            var eventName = eventArgs.RoutingKey;
            var message = Encoding.UTF8.GetString(eventArgs.Body.Span);

            _logger.LogInformation("Response_Consumer_Received");

            try
            {
                if (message.ToLowerInvariant().Contains("throw-fake-exception"))
                {
                    throw new InvalidOperationException($"Fake exception requested: \"{message}\"");
                }

                await ProcessEvent(eventName, message, useReply: true);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, "----- ERROR Processing message \"{Message}\"", message);
            }

            // Even on exception we take the message off the queue.
            // in a REAL WORLD app this should be handled with a Dead Letter Exchange (DLX). 
            // For more information see: https://www.rabbitmq.com/dlx.html
            _consumerChannel.BasicAck(eventArgs.DeliveryTag, multiple: false);
        }


        private IModel CreateConsumerChannel()
        {
            if (!_persistentConnection.IsConnected)
            {
                _persistentConnection.TryConnect();
            }

            _logger.LogTrace("Creating RabbitMQ consumer channel");

            var channel = _persistentConnection.CreateModel();

            channel.ExchangeDeclare(exchange: BrokerName,
                                    type: "direct");

            channel.QueueDeclare(queue: _queueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);

            channel.CallbackException += (sender, ea) =>
            {
                _logger.LogWarning(ea.Exception, "Recreating RabbitMQ consumer channel");

                _consumerChannel.Dispose();
                _consumerChannel = CreateConsumerChannel();
                StartBasicConsume();
            };

            return channel;
        }

        private IModel CreateResponseChannel()
        {
            if (IsReplyNeeded)
            {
                if (!_persistentConnection.IsConnected)
                {
                    _persistentConnection.TryConnect();
                }

                _logger.LogTrace("Creating RabbitMQ Response Channel");

                var channel = _persistentConnection.CreateModel();

                channel.ExchangeDeclare(exchange: ResponseBrokerName,
                                        type: "direct");

                channel.QueueDeclare(queue: _queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                channel.CallbackException += (sender, ea) =>
                {
                    _logger.LogWarning(ea.Exception, "Recreating RabbitMQ response channel");

                    ReplyChannel.Dispose();
                    ReplyChannel = CreateResponseChannel();
                    StartBasicConsume(true);
                };

                return channel;
            }
            else
            {
                return null;
            }
        }

        private async Task ProcessEvent(string eventName, string message, bool useReply = false)
        {
            _logger.LogTrace("Processing RabbitMQ event: {EventName}", eventName);

            if (useReply)
            {
                if (HasDelegateBeenRegistered)
                {
                    _logger.LogInformation("Reply was received and will begin provessing via the response handler");

                    var integrationEvent = JsonSerializer.Deserialize<DistributedFileReadIntegrationEvent>(message, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });

                    await handle.Invoke(integrationEvent);

                    HasChanged = true;

                    return;
                }
            }

            if (_subsManager.HasSubscriptionsForEvent(eventName))
            {
                using (var scope = _autofac.BeginLifetimeScope(AutoFacScopeName))
                {
                    var subscriptions = _subsManager.GetHandlersForEvent(eventName);
                    foreach (var subscription in subscriptions)
                    {
                        if (subscription.IsDynamic)
                        {
                            var handler = scope.ResolveOptional(subscription.HandlerType) as IDynamicIntegrationEventHandler;
                            if (handler == null) continue;
                            using dynamic eventData = JsonDocument.Parse(message);
                            await Task.Yield();
                            await handler.Handle(eventData);
                        }
                        else
                        {
                            var handler = scope.ResolveOptional(subscription.HandlerType);
                            if (handler == null) continue;
                            var eventType = _subsManager.GetEventTypeByName(eventName);
                            var integrationEvent = JsonSerializer.Deserialize(message, eventType, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                            var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(eventType);

                            await Task.Yield();
                            await (Task)concreteType.GetMethod("Handle").Invoke(handler, new object[] { integrationEvent });
                        }
                    }
                }
            }
            else
            {
                _logger.LogWarning("No subscription for RabbitMQ event: {EventName}", eventName);
            }
        }

        public void SubscribeReply(Guid iD)
        {
            _logger.LogInformation(
                "Subscribing to: exhangeKey:{ResponseBrokerName}, routingKey:{StringID}", ResponseBrokerName, iD.ToString());

            ReplyChannel = CreateResponseChannel();

            DoInternalSubscription(subscribeToReply: true, iD: iD);

            StartBasicConsume(basicReplyConsume: true);
        }

        public void UnsubscribeReply(string eventName)
        {
            throw new NotImplementedException();
        }

    }
}
