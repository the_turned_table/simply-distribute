﻿using KubeMQ.SDK.csharp.CommandQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static KubeMQ.SDK.csharp.CommandQuery.Responder;

namespace DistributeSecondary.Builders
{
    public interface IBuilderSubscriber
    {
        public IBuilderSubscriber SetChannelName(string channelName);

        public IBuilderSubscriber SetClientID(string clientID);

        public IBuilderSubscriber SetServerAddress(string serverAddress);

        public IBuilderSubscriber SetOnReceived(RespondDelegate onReceived);

        public IBuilderSubscriber SetOnError(HandleCommandQueryErrorDelegate onError);

        public IBuilderSubscriber SetCancellationToken(CancellationToken cancellationToken);

        public Responder Build();
    }
}
