﻿using KubeMQ.SDK.csharp.CommandQuery;
using KubeMQ.SDK.csharp.Subscription;
using System.Threading;
using static KubeMQ.SDK.csharp.CommandQuery.Responder;

namespace DistributeSecondary.Builders
{
    public class KubeMQSubscriberBuilder : IBuilderSubscriber
    {
        private string ChannelName;

        private string ClientID;

        private string ServerAddress;

        private RespondDelegate OnReceived;

        private HandleCommandQueryErrorDelegate OnError;

        private CancellationToken CancellationToken;

        public IBuilderSubscriber SetChannelName(string channelName)
        {
            ChannelName = channelName;

            return this;
        }

        public IBuilderSubscriber SetClientID(string clientID)
        {
            ClientID = clientID;

            return this;
        }

        public IBuilderSubscriber SetServerAddress(string serverAddress)
        {
            ServerAddress = serverAddress;

            return this;
        }

        public IBuilderSubscriber SetOnReceived(RespondDelegate onReceived)
        {
            OnReceived = onReceived;

            return this;
        }

        public IBuilderSubscriber SetOnError(HandleCommandQueryErrorDelegate onError)
        {
            OnError = onError;

            return this;
        }

        public IBuilderSubscriber SetCancellationToken(CancellationToken cancellationToken)
        {
            CancellationToken = cancellationToken;

            return this;
        }

        public Responder Build()
        {
            var responder = new Responder(ServerAddress);

            responder.SubscribeToRequests(new SubscribeRequest() 
                {
                    Channel = ChannelName,
                    SubscribeType = SubscribeType.Commands,
                    ClientID = ClientID
                },
                OnReceived,
                OnError,
                CancellationToken);

            return responder;
        }
    }
}
