﻿using KubeMQ.SDK.csharp.CommandQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.Builders
{
    public class KubeMQPublisherCommandBuilder : IBuilderChannelPublisher
    {
        private string ChannelName;

        private string ClientID;

        private string ServerName;

        public Channel Build()
        {

            return new Channel(new ChannelParameters
            {
                RequestsType = RequestType.Query,
                Timeout = 1000,
                ChannelName = ChannelName,
                ClientID = ClientID,
                KubeMQAddress = ServerName
            });
        }

        public IBuilderChannelPublisher SetChannelName(string channelName)
        {
            ChannelName = channelName;

            return this;
        }

        public IBuilderChannelPublisher SetClientID(string clientID)
        {
            ClientID = clientID;

            return this;
        }

        public IBuilderChannelPublisher SetServerName(string serverName)
        {
            ServerName = serverName;

            return this;
        }
    }
}
