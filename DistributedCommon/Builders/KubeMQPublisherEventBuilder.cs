﻿using KubeMQ.SDK.csharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.Builders
{
    public class KubeMQPublisherEventBuilder : IBuilderEventPublisher
    {
        private string ChannelName;

        private string ClientID;

        private string ServerName;

        public Channel Build()
        {

            return new Channel(new ChannelParameters
            {
                ChannelName = ChannelName,
                ClientID = ClientID,
                KubeMQAddress = ServerName
            });
        }

        public IBuilderEventPublisher SetChannelName(string channelName)
        {
            ChannelName = channelName;

            return this;
        }

        public IBuilderEventPublisher SetClientID(string clientID)
        {
            ClientID = clientID;

            return this;
        }

        public IBuilderEventPublisher SetServerName(string serverName)
        {
            ServerName = serverName;

            return this;
        }
    }
}
