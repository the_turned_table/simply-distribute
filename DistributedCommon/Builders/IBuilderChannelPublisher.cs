﻿using KubeMQ.SDK.csharp.CommandQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.Builders
{
    public interface IBuilderChannelPublisher
    {
        public IBuilderChannelPublisher SetChannelName(string channelName);

        public IBuilderChannelPublisher SetClientID(string clientID);

        public IBuilderChannelPublisher SetServerName(string serverName);

        public Channel Build();
    }
}
