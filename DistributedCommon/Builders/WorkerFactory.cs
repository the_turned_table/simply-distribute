﻿using Distributed.Worker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Distributed.Builders
{
    public interface IWorkerFactory
    {
        public IFileIOWorker BuildFileIOWorker(string FileDirectory);

        public IFileIOWorker BuildFileIOWorker();

        public IFileNamingWorker BuildFileNamingWorker(string FileDirectory);
    }

    public class WorkerFactory : IWorkerFactory
    {
        public WorkerFactory() { }

        public IFileIOWorker BuildFileIOWorker(string FileDirectory)
        {
            return new FileIOWorker(FileDirectory);
        }

        public IFileIOWorker BuildFileIOWorker()
        {
            return new FileIOWorker();
        }

        public IFileNamingWorker BuildFileNamingWorker(string FileDirectory)
        {
            var worker = new FileNamingWorker();
            worker.RegisterFileDictionary(FileDirectory);

            return worker;
        }
    }
}
