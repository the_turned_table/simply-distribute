﻿using KubeMQ.SDK.csharp.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.Builders
{
    public interface IBuilderEventPublisher
    {
        public IBuilderEventPublisher SetChannelName(string channelName);

        public IBuilderEventPublisher SetClientID(string clientID);

        public IBuilderEventPublisher SetServerName(string serverName);

        public Channel Build();
    }
}
