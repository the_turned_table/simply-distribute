﻿using System.Collections.Generic;

namespace Distributed.Worker
{
    public interface IFileNamingWorker
    {
        public void RegisterFileDictionary(string fileName);

        List<string> DoWork(string fileName);
    }
}
