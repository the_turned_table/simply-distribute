﻿using System.Collections.Generic;

namespace Distributed.Worker
{
    public class FileNamingWorker : IFileNamingWorker
    {
        public List<string> fileNames = new List<string>()
        {
            "File1.txt",
            "File2.txt",
            "File3.txt",
            "File4.txt"
        };

        public Dictionary<string, List<string>> fileDictionary = new Dictionary<string, List<string>>();

        public void RegisterFileDictionary(string fileName)
        {
            fileDictionary.Add(fileName, fileNames);
        }

        public List<string> DoWork(string fileName)
        {
            return fileDictionary[fileName];
        }
    }
}
