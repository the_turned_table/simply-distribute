﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Distributed.Worker
{
    public class FileIOWorker: IFileIOWorker
    {
        private string FileDirectory;

        public FileIOWorker() { }

        public FileIOWorker(string fileDirectory)
        {
            FileDirectory = fileDirectory;
        }

        public void RegisterFile(string file)
        {
            FileDirectory = file;
        }

        public async IAsyncEnumerable<byte[]> DoReadAsync()
        {
            List<String> lines = new List<string>();

            await foreach (var line in ReadAsync())
            {
                yield return line;
            }
        }

        private async IAsyncEnumerable<byte[]> ReadAsync()
        {
            using (FileStream Stream = File.Open(FileDirectory, FileMode.Open))
            {

                var buffer = new byte[Stream.Length];

                await Stream.ReadAsync(buffer, 0, (int)Stream.Length);

                yield return buffer;
            }
        }

        public async Task DoWriteAsync(byte[] lines)
        {
            await File.WriteAllBytesAsync(FileDirectory, lines);
        }

        public byte[] DoReadNBytes(string fileName,
                                   int blockSize,
                                   int blockBegin,
                                   int blockEnd)
        {
            var fileBlock = new byte[blockSize];

            using (BinaryReader reader = new BinaryReader(new FileStream(fileName, FileMode.Open)))
            {
                reader.BaseStream.Seek(blockEnd, SeekOrigin.Begin);
                reader.Read(fileBlock, 0, blockSize);
            }

            return fileBlock;
        }

    }
}
