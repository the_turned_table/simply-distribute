﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Distributed.Worker
{
    public interface IFileIOWorker
    {
        public void RegisterFile(string file);

        public Task DoWriteAsync(byte[] lines);

        public IAsyncEnumerable<byte[]> DoReadAsync();

        public byte[] DoReadNBytes(string fileName,
                                                     int blockSize,
                                                     int blockBegin,
                                                     int blockEnd);
    }
}
