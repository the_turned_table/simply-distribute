﻿using System.Threading.Tasks;

namespace DistributedCommon.EventBus.Abstractions
{
    public interface IDynamicIntegrationEventHandler
    {
        Task Handle(dynamic eventData);
    }
}
