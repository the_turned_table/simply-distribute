﻿using DistributedCommon.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributedCommon.EventBus.Abstractions
{
    public interface IEventBusOldImplementation
    {
        void Publish(IntegrationEvent @event);

        void PublishResponse(IntegrationEvent @event, Guid iD, bool isReplyNeeded);

        void Subscribe<T, TH>()
            where T : IntegrationEvent
            where TH : IIntegrationEventHandler<T>;

        void SubscribeReply(Guid iD);

        void UnsubscribeReply(string eventName);

        void SubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        void UnsubscribeDynamic<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        void Unsubscribe<T, TH>()
            where TH : IIntegrationEventHandler<T>
            where T : IntegrationEvent;

    }
}
