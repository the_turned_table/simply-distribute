﻿using DistributedCommon.EventBus.Abstractions;
using DistributedCommon.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DistributedCommon.EventBus.InMemoryEventBusSubscriptionsManager;

namespace DistributedCommon.EventBus
{
    public interface IEventBusSubscriptionsManager
    {
        bool IsEmpty { get; }
        
        event EventHandler<string> OnEventRemoved;

        void AddDynamicSubscription<H>(string eventName)
            where H : IDynamicIntegrationEventHandler;

        void AddSubscription<T, H>()
            where T : IntegrationEvent
            where H : IIntegrationEventHandler<T>;

        void RemoveSubscription<T, H>()
            where T : IntegrationEvent
            where H : IIntegrationEventHandler<T>;

        void RemoveDynamicSubscription<TH>(string eventName)
            where TH : IDynamicIntegrationEventHandler;

        bool HasSubscriptionsForEvent<T>() 
            where T : IntegrationEvent;

        void SubscribeReply(Guid id);

        void UnSubscripeReply(Guid id);

        bool HasSubscriptionsForReply(Guid id);

        bool HasSubscriptionsForEvent(string eventName);

        Type GetEventTypeByName(string eventName);

        void Clear();

        IEnumerable<SubscriptionInfo> GetHandlersForEvent<T>() 
            where T : IntegrationEvent;

        IEnumerable<SubscriptionInfo> GetHandlersForEvent(string eventName);

        string GetEventKey<T>();
    }
}
