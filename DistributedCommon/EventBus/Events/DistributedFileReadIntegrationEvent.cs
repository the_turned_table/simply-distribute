﻿using DistributedCommon.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Distribute.IntegrationEvents.Events
{
    public record DistributedFileReadIntegrationEvent : IntegrationEvent
    {
        public string FileName { get; private init; }

        public int BytesPartition { get; private init; }

        public int Order { get; private init; }

        public string Data { get; private init; }

        public Guid FileGuid { get; private init; }

        public DistributedFileReadIntegrationEvent(string fileName, int bytesPartition, int order, string data, Guid fileGuid)
        {
            FileName = fileName;
            BytesPartition = bytesPartition;
            Order = order;
            Data = data;
            FileGuid = fileGuid;
        }
    }
}
