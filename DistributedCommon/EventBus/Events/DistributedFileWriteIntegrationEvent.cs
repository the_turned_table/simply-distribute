﻿using DistributedCommon.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DistributeSecondary.IntegrationEvents.Events
{
    public record DistributedFileWriteIntegrationEvent : IntegrationEvent
    {
        public string FileName { get; private init; }

        public byte[] FileContents { get; private init; }

        public DistributedFileWriteIntegrationEvent(string fileName, byte[] fileContents)
        {
            FileName = fileName;
            FileContents = fileContents;
        }
    }
}
