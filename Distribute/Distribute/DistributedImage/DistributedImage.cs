﻿using CSharpTest.Net.Collections;
using CSharpTest.Net.Serialization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Distribute.DistributedImage
{
    public class DistributedImage
    {
        public BPlusTree<string, string> InMemoryFile;

        public DistributedImage(BPlusTree<string, string> inMemoryFile)
        {
            InMemoryFile = inMemoryFile;
        }

        public List<string> GetFileInformation(string fileLocation)
        {
            List<string> fileMap;
            string fileLocations = string.Empty;
            if (InMemoryFile.TryGetValue(fileLocation, out fileLocations) == true)
            {
                fileMap = new List<string>(fileLocations.Split(new char[] { ',' }));
            }
            else
            {
                fileMap = new List<string>();
            }

            return fileMap;
        }

        public bool AddFile(string fileName, string fileLocations)
        {
            return InMemoryFile.TryAdd(fileName, fileLocations);
        }

        public bool DoesFileExist(string fileName)
        {
            InMemoryFile.TryGetValue(fileName, out string fileExists);

            return string.IsNullOrEmpty(fileExists);
        }

        public bool DeleteFile(string fileName)
        {
            InMemoryFile.Remove(fileName);

            return DoesFileExist(fileName);
        }

        public static class Factory
        {
            public static DistributedImage Build()
            {
                BPlusTree<string, string>.Options options = new BPlusTree<string, string>.Options(
                    PrimitiveSerializer.String, PrimitiveSerializer.String, StringComparer.Ordinal)
                {
                    CreateFile = CreatePolicy.IfNeeded,
                    FileName = @"/tmp/distributed_image/distributed_image.dat",
                };

                return new DistributedImage(new BPlusTree<string, string>(options));
            }
        }
    }

    public class DistributedFileInformation
    {
        public List<string> FileBlockLocation;


    }
}
