﻿using Distribute.DistributedImage;
using Distribute.IntegrationEvents.Events;
using Distributed.Builders;
using DistributedCommon.Builders;
using DistributedCommon.EventBus.Abstractions;
using DistributedCommon.EventBusRabbitMQ;
using DistributeSecondary.IntegrationEvents.Events;
using KubeMQ.SDK.csharp.Events;
using KubeMQ.SDK.csharp.Tools;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Distributed.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DistributedWriteController : ControllerBase
    {

        private readonly ILogger<DistributedWriteController> _logger;

        private readonly IWorkerFactory WorkerFactory;

        private string FileName = @"File.txt";

        private readonly EventBusServiceProvider eventBusServiceProvider;

        private readonly DistributedImage DistributedImage;

        public DistributedWriteController(ILogger<DistributedWriteController> logger, IWorkerFactory workerFactory, EventBusServiceProvider eventBusServiceProvider, DistributedImage distributedImage)
        {
            _logger = logger;

            WorkerFactory = workerFactory;

            this.eventBusServiceProvider = eventBusServiceProvider;

            this.DistributedImage = distributedImage;
        }

        //public override Task<DistributedWriteReply> WriteFile(IAsyncStreamReader<DistributedWriteRequest> requestStream, ServerCallContext context)
        //{
        //    return base.WriteFile(requestStream, context);
        //}

        [HttpGet]
        [Route("GetFile")]
        public async Task GetFile(string FileName)
        {
            List<string> FileNames = eventBusServiceProvider.GetAvailableNodes();

            FileNames.ForEach( (fileName) =>
            {
                _logger.LogInformation(fileName);
            });

            List<Task> fileTasks = new List<Task>(FileNames.Count);

            var readEvent = new DistributedFileReadIntegrationEvent(FileName, 0, 0, string.Empty, Guid.NewGuid());

            eventBusServiceProvider.RegisterHandleToAllNodes(FileNames, WriteToResponse, readEvent.FileGuid);

            eventBusServiceProvider.PublishEventToAllEventBus(FileNames, readEvent);

            var list = eventBusServiceProvider.AwaitAllResults(FileNames);

            list.ForEach(async value =>
            {
               await Response.Body.WriteAsync(JsonSerializer.SerializeToUtf8Bytes(value, value.GetType(), new JsonSerializerOptions
               {
                   WriteIndented = true
               }));
            });

            await this.Response.Body.FlushAsync();
        }

        private Task<DistributedFileReadIntegrationEvent> WriteToResponse(DistributedFileReadIntegrationEvent @event)
        {
            _logger.LogInformation("Received Event");

            return Task.FromResult(@event);
        }

        [HttpPost]
        [Route("WriteFile")]
        public async void WriteFile()
        {
            string fileName = "input.txt";

            if (!DistributedImage.DoesFileExist(fileName))
            {
                this.Response.StatusCode = 403;

                return;
            }

            try
            {
                List<string> fileMap = eventBusServiceProvider.GetAvailableNodes();

                using (StreamReader sr = new StreamReader("input.txt"))
                {
                    var buffer = new char[sr.BaseStream.Length];

                    await sr.ReadAsync(buffer, 0, buffer.Length);

                    eventBusServiceProvider.PublishEventToAllEventBus(fileMap, new DistributedFileWriteIntegrationEvent(
                        "input.txt", Encoding.ASCII.GetBytes(buffer)));
                }

            } 
            catch (Exception e)
            {
                _logger.LogInformation("Excpeption occurred while processing");
            }
        }
    }
}
