using System;

namespace DistributeSecondary
{
    public class FileRequest
    {
        public string FileName { get; set; }

        public byte[] Contents { get; set; }

        public int Partitition { get; set; }
    }
}
