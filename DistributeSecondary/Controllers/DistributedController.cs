﻿using Distributed.Builders;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributeSecondary.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DistributedController : ControllerBase
    {
        private readonly ILogger<DistributedController> Logger;

        private IWorkerFactory WorkerFactory;

        public DistributedController(ILogger<DistributedController> logger, IWorkerFactory WorkerFactory)
        {
            Logger = logger;
            this.WorkerFactory = WorkerFactory;
        }

        [HttpGet]
        [Route("HeartBeat")]
        public bool HeartBeat()
        {
            return true;
        }

        [HttpGet]
        public async Task GetFile(string fileName)
        {
            // Create MasterDataOrchestrator

            // Publish 1 event 
            var sequential = WorkerFactory.BuildFileIOWorker(fileName).DoReadAsync();

            await foreach (var bytes in sequential)
            {
                await Response.Body.WriteAsync(bytes, 0, bytes.Length);
            }

            await Response.Body.FlushAsync();
        }

        [HttpPost]
        public bool WriteFile(FileRequest fileRequest)
        {
            var fileIOWorker = WorkerFactory.BuildFileIOWorker();

            fileIOWorker.RegisterFile(fileRequest.FileName);

            fileIOWorker.DoWriteAsync(fileRequest.Contents);

            return true;
        }
    }
}
