﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DistributeSecondary.IntegrationEvents.Events;
using DistributedCommon.EventBus.Abstractions;
using Microsoft.Extensions.Logging;
using System.Text;
using Distributed.Builders;

namespace DistributeSecondary.IntegrationEvents.EventHandling
{
    public class DistributedWriteIntegrationEventHandler : IIntegrationEventHandler<DistributedFileWriteIntegrationEvent>
    {
        private readonly ILogger<DistributedWriteIntegrationEventHandler> Logger;

        private IWorkerFactory WorkerFactory;

        public DistributedWriteIntegrationEventHandler(ILogger<DistributedWriteIntegrationEventHandler> Logger, IWorkerFactory WorkerFactory)
        {
            this.Logger = Logger;

            this.WorkerFactory = WorkerFactory;
        }

        public async Task Handle(DistributedFileWriteIntegrationEvent @event)
        {
            Logger.LogInformation("Event has been received");

            var fileIOWorker = WorkerFactory.BuildFileIOWorker();

            fileIOWorker.RegisterFile(@event.FileName);

            await fileIOWorker.DoWriteAsync(@event.FileContents);
        }
    }
}
