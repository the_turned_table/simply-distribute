﻿using Distribute.IntegrationEvents.Events;
using DistributedCommon.EventBus.Abstractions;
using DistributeSecondary.IntegrationEvents.Events;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DistributeSecondary.IntegrationEvents.EventHandling
{
    public class DistributedFileReadIntegrationEventHandler : IIntegrationEventHandler<DistributedFileReadIntegrationEvent>
    {
        private readonly ILogger<DistributedFileReadIntegrationEventHandler> Logger;

        private readonly IEventBus eventBus;

        public DistributedFileReadIntegrationEventHandler(ILogger<DistributedFileReadIntegrationEventHandler> logger, IEventBus eventBus)
        {
            Logger = logger;

            this.eventBus = eventBus;
        }

        public async Task Handle(DistributedFileReadIntegrationEvent @event)
        {
            Logger.LogInformation("Event Received");

            using (FileStream SourceStream = File.Open("input.txt", FileMode.Open))
            {
                var buffer = new byte[SourceStream.Length];

                await SourceStream.ReadAsync(buffer, 0, (int)SourceStream.Length);

                var fileReadEvent = new DistributedFileReadIntegrationEvent(@event.FileName, @event.BytesPartition, @event.Order, Encoding.ASCII.GetString(buffer), @event.FileGuid);

                eventBus.PublishResponse(fileReadEvent, fileReadEvent.FileGuid, isReplyNeeded: true);
            }

            await Task.CompletedTask;
        }
    }
}
