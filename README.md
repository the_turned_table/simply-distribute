# Simply Distribute

This is a distributed file system written by me in C#.

The application is designed to be run in docker containers.

The decision to write the application in this format was to create an easy to deploy application that can be highly configurable.

The project has a dependency on RabbitMQ messaging service as the queueing service. RabbitMQ is used to send messages within the application to respond to file requests. Messages are routed based off the FileGuid which ensures that the requests are always unique and a the likelihood of ID's clacheing is very low.
